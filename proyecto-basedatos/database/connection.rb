require 'activerecord'

module Connection
    def self.connect
        ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: 'awesome_market')
    end
end