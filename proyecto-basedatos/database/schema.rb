module Schema
    def self.create
        ActiveRecord::Schema.define do 
            create_table :user, force: true do |table|
                table.string :name, :lastname, :birth_date, :id_number, :phone_number, :username, :password, :e_mail
                table.integer :date 
            end
        end
    end
end